# Moonlight Theme

![preview](preview.svg)

A Firefox theme inspired by the [Moonlight color palette](https://raw.githubusercontent.com/eduardhojbota/moonlight-userChrome/master/preview.jpg).
